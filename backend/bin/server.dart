import 'package:backend/config/config.dart';
import 'package:backend/server/server.dart';

void main(List<String> args) async {
  Server.bootstrap(controllers)
      .then((server) => print(
          'Server listen on http://${server.address.host}:${server.port}'))
      .catchError(print);
}
