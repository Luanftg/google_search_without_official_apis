import 'package:backend/app/api/api.dart';
import 'package:test/test.dart';

import '../../../mock.dart';

void main() {
  late SearchController searchController;
  late MockSearchFromQueryService mockSearchFromQueryService;
  setUp(() {
    mockSearchFromQueryService = MockSearchFromQueryService();
    searchController = SearchController(mockSearchFromQueryService);
  });
  test('should contain a route /search', () async {
    expect(searchController.route, '/search');
  });

  test('should contain a key GET/<query> for GetSearchHandler', () async {
    expect(searchController.handlers['GET/<query>'], isA<GetSearchHandler>());
  });
}
