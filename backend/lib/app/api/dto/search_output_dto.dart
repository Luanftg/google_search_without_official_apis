import 'package:backend/app/domain/entities/search_entity.dart';

class SearchOutputDTO {
  final String title;
  final String url;

  SearchOutputDTO({required this.title, required this.url});

  Map<String, dynamic> toMap() {
    return {'title': title, 'url': url};
  }

  static SearchOutputDTO fromMap(Map<String, dynamic> map) {
    return SearchOutputDTO(title: map['title'], url: map['url']);
  }

  static SearchOutputDTO toDTO(SearchEntity searchEntity) {
    return SearchOutputDTO(title: searchEntity.title, url: searchEntity.url);
  }

  static List<SearchOutputDTO> toCollection(List<SearchEntity> list) {
    return list.map(SearchOutputDTO.toDTO).toList();
  }

  static SearchEntity toEntity(Map<String, dynamic> map) {
    return SearchEntity(title: map['title'], url: map['url']);
  }
}
