part of api;

class SearchController implements Controller {
  final SearchFromQueryService searchFromQueryService;

  SearchController(this.searchFromQueryService);

  @override
  Map<String, Handler> get handlers =>
      {'GET': GetSearchHandler(searchFromQueryService)};

  @override
  String get route => '/search/<query>';
}
