part of api;

class GetSearchHandler implements Handler {
  final SearchFromQueryService searchFromQueryService;

  GetSearchHandler(this.searchFromQueryService);
  @override
  Future<ResponseHandler> call(RequestParams requestParams) async {
    final list = await searchFromQueryService(requestParams.routeParam ?? '');
    return ResponseHandler(
        status: StatusHandler.ok, body: SearchOutputDTO.toCollection(list));
  }
}
