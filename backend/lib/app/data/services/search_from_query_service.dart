part of data;

class SearchFromQueryService implements SearchFromQueryUsecase {
  // final BeautifulSoup beautifulSoup;
  final Client client;

  SearchFromQueryService({required this.client});

  @override
  Future<List<SearchEntity>> call(String query) async {
    final url = Uri.parse('https://www.google.com.br/search?q=$query');

    final response2 = await client.get(url);

    if (response2.statusCode == 200) {
      final soup = BeautifulSoup(
        response2.body,
      );

      final listdivG = soup.findAll('div', class_: 'g');
      final items = listdivG
          .where((element) => element.className == 'egMi0 kCrYT')
          .toList();

      final resultList = <Map<String, String>>[];

      for (final item in items) {
        final title = item.h3?.text;

        final fullURL = item.a?.attributes['href'];
        final index = fullURL?.indexOf('&');
        final url = item.a?.attributes['href']?.substring(7, index);

        if (title != null && url != null) {
          resultList.add({
            'title': title,
            'url': url,
          });
        }
      }

      return resultList.map(SearchOutputDTO.toEntity).toList();
    } else {
      throw Exception('Erro na solicitação HTTP: ${response2.statusCode}');
    }
  }
}
