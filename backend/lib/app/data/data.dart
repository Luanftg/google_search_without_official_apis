library data;

import 'package:backend/app/domain/entities/search_entity.dart';
import 'package:beautiful_soup_dart/beautiful_soup.dart';
import 'package:http/http.dart';

import '../api/dto/search_output_dto.dart';
import '../domain/domain.dart';

part 'services/search_from_query_service.dart';
