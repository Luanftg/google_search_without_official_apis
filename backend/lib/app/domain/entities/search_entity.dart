class SearchEntity {
  final String title;
  final String url;

  SearchEntity({required this.title, required this.url});
}
