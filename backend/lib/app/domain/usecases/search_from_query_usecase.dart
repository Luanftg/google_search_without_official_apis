part of domain;

abstract class SearchFromQueryUsecase {
  Future<List<SearchEntity>> call(String query);
}
