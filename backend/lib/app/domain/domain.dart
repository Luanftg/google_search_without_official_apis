library domain;

import 'entities/search_entity.dart';

part 'entities/entities.dart';
part 'usecases/search_from_query_usecase.dart';
