part of server;

class ShelfAdapter {
  List<Controller> controllers;

  ShelfAdapter({required this.controllers});

  void handler(Router router) {
    for (final controller in controllers) {
      _handler(controller, router);
    }
  }

  void _handler(Controller controller, Router router) {
    final String route = controller.route;
    final handlers = controller.handlers;

    for (final entry in handlers.entries) {
      final String verb = entry.key;
      // if (entry.key.contains('/')) {
      //   verb = entry.key.substring(0, 3);
      // } else {
      //   verb = entry.key;
      // }

      router.add(verb, route, (shelf.Request request) async {
        final payload = await request.readAsString();
        final routeParam = request.params['query'];
        final responseHandler = await entry.value(RequestParams(
            body: payload.isEmpty ? null : jsonDecode(payload),
            routeParam: routeParam));
        switch (responseHandler.status) {
          case StatusHandler.ok:
            return ResponseJSON.ok(responseHandler.body);
          case StatusHandler.badRequest:
            return ResponseJSON.badRequest(responseHandler.body);
          default:
            return shelf.Response.internalServerError();
        }
      });
    }
  }
}
