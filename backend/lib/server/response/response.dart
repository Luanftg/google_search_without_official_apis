part of server;

class ResponseJSON extends shelf.Response {
  ResponseJSON.ok(dynamic body)
      : super.ok(
          body is List
              ? jsonEncode(body.map((item) => item.toMap()).toList())
              : jsonEncode(body.toMap()),
        );

  ResponseJSON.badRequest(dynamic body)
      : super.badRequest(body: jsonEncode(body.toMap()));
}
