import 'package:flutter/material.dart';
import 'package:frontend/app/core/core.dart';

import 'app/presentations/presentation.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FrontEnd - Search App',
      debugShowCheckedModeBanner: false,
      theme: ThemeConfig.theme,
      home: const HomePage(title: 'Google Search App'),
    );
  }
}
