part of core;

extension SizeExtensions on BuildContext {
  double get screenWidth => MediaQuery.of(this).size.width;
  double get screenHeight => MediaQuery.of(this).size.height;

  double percenteWidth(double percent) => screenWidth * percent;
  double percentHeight(double percent) => screenHeight * percent;
}
