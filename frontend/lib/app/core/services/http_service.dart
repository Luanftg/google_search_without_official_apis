part of core;

abstract class HttpService {
  Future<Response<T>> get<T>(String path);
}
