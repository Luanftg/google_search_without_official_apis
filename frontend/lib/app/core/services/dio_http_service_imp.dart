part of core;

class DioHttpServiceImp implements HttpService {
  late Dio _dio;
  DioHttpServiceImp() {
    _dio = Dio();
  }

  @override
  Future<Response<T>> get<T>(
    String path,
  ) {
    return _dio.get<T>(
      path,
    );
  }
}
