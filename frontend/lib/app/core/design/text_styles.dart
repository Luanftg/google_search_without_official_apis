part of core;

class TextStyles {
  static TextStyles? _instance;
  TextStyles._();

  static TextStyles get i => _instance ??= TextStyles._();

  String get font => 'Roboto';

  TextStyle get textLight =>
      TextStyle(fontWeight: FontWeight.w300, fontFamily: font);
  TextStyle get errorText => const TextStyle(
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.underline,
      );
}

extension TextStylesExtensions on BuildContext {
  TextStyles get textStyles => TextStyles.i;
}
