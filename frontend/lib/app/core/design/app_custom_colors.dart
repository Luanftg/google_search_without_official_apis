part of core;

class AppCustomColors {
  static AppCustomColors? _instance;
  AppCustomColors._();

  static AppCustomColors get i => _instance ??= AppCustomColors._();

  Color get primary => Colors.deepPurple;
  Color get black => const Color(0xff292B2D);
  Color get white => const Color(0xffFFFFFF);
  Color get grey1 => const Color(0xffECECEC);
  Color get grey2 => const Color(0xffCACBCC);
}

extension AppCustomcolorsExtensions on BuildContext {
  AppCustomColors get colors => AppCustomColors.i;
}
