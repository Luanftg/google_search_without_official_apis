library core;

import 'package:dependency_injector/dependency_injector.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../data/data.dart';
import '../domain/domain.dart';
import '../presentations/presentation.dart';

part 'services/service.dart';
part 'services/http_service.dart';
part 'services/dio_http_service_imp.dart';
part 'extensions/size_extensions.dart';
part 'design/app_custom_colors.dart';
part 'design/text_styles.dart';
part 'design/theme_config.dart';
part 'injects/injects.dart';
