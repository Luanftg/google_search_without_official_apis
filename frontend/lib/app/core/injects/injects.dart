part of core;

class Injects {
  static Future<DependencyInjector> initialize() async {
    final di = DependencyInjector();
    WidgetsFlutterBinding.ensureInitialized();

    //Client
    di.register<HttpService>(() => DioHttpServiceImp());
    //Datasource
    di.register<GetSearchByQueryDatasource>(
        () => GetSearchByQueryDatasourceImp(di.get<HttpService>()));
    //Repository
    di.register<GetSearchByQueryRepository>(() =>
        GetSearchByQueryRepositoryImp(di.get<GetSearchByQueryDatasource>()));
    //Usecase
    di.register<GetSearchByQueryUsecase>(
        () => GetSearchByQueryUsecaseImp(di.get<GetSearchByQueryRepository>()));
    //Component - State Manager
    di.register<SearchComponent>(
      () => SearchComponent(di.get<GetSearchByQueryUsecase>()),
    );

    return di;
  }
}
