library presentation;

import 'dart:async';

import 'package:dependency_injector/dependency_injector.dart';
import 'package:flutter/material.dart';
import 'package:frontend/app/presentations/views/loading_search__view.dart';

import 'package:responsive_layout/responsive_layout.dart';
import 'package:url_launcher/url_launcher.dart';

import '../core/core.dart';

import '../domain/domain.dart';

part 'widgets/custom_list_tile.dart';
part 'widgets/error_text.dart';
part 'pages/home_page.dart';
part 'views/search_view.dart';
part 'views/sucess_search_view.dart';
part 'components/search_component.dart';
part 'widgets/custom_return_button.dart';
