part of presentation;

enum SearchEvent { initial, loading, search }

abstract class SearchState {}

class InitialSearchState implements SearchState {}

class LoadingSearchState implements SearchState {}

class SucessSearchState implements SearchState {
  final List<SearchEntity> list;
  SucessSearchState(this.list);
}

class SearchComponent {
  List<SearchEntity> listOfSearchModel = [];
  String _query = '';
  final GetSearchByQueryUsecase _getSearchByQueryUsecase;

  final StreamController<SearchState> _stateController =
      StreamController<SearchState>.broadcast();
  final StreamController<SearchEvent> _eventController =
      StreamController<SearchEvent>.broadcast();

  InitialSearchState get initialData => InitialSearchState();
  Stream<SearchState> get state => _stateController.stream;

  SearchComponent(
    this._getSearchByQueryUsecase,
  ) {
    _eventController.stream.listen((event) async {
      await _handleEvent(event);
    });
    debugPrint(
        'SearchComponent -> Objeto $hashCode criado em ${DateTime.now()}');
  }

  void close() {
    _eventController.close();
    _stateController.close();
  }

  Future<void> search(String query) async {
    emitLoadingState();
    _query = query;
    final response = await _getSearchByQueryUsecase(_query);
    listOfSearchModel = response;
  }

  void emitInitialState() {
    _eventController.sink.add(SearchEvent.initial);
  }

  void emitLoadingState() {
    _eventController.sink.add(SearchEvent.loading);
  }

  void emitSucessState() {
    _eventController.sink.add(SearchEvent.search);
  }

  Future<void> _handleEvent(SearchEvent event) async {
    if (event == SearchEvent.search) {
      _stateController.sink.add(SucessSearchState(listOfSearchModel));
    } else if (event == SearchEvent.initial) {
      _stateController.sink.add(InitialSearchState());
    } else if (event == SearchEvent.loading) {
      _stateController.sink.add(LoadingSearchState());
    }
  }
}
