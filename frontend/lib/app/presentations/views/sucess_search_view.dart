part of presentation;

class SucessSearchView extends StatefulWidget {
  const SucessSearchView(this._searchOutputModel, {super.key});
  final List<SearchEntity> _searchOutputModel;

  @override
  State<SucessSearchView> createState() => _SucessSearchViewState();
}

class _SucessSearchViewState extends State<SucessSearchView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Center(
        child: ListView(
          children: widget._searchOutputModel.isEmpty
              ? [
                  ErrorText(query: _searchComponent._query),
                  const CustomReturnButton(),
                ]
              : List.generate(
                  widget._searchOutputModel.length + 1,
                  (index) {
                    if (index == widget._searchOutputModel.length - 1) {
                      return const CustomReturnButton();
                    }
                    if (index == widget._searchOutputModel.length) {
                      return const SizedBox(height: 16);
                    }
                    return CustomListTile(
                      title: widget._searchOutputModel[index].title,
                      url: widget._searchOutputModel[index].url,
                    );
                  },
                ),
        ),
      ),
    );
  }
}
