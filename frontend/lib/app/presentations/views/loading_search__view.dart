import 'package:flutter/material.dart';
import 'package:frontend/app/presentations/presentation.dart';

class LoadingSearchView extends StatefulWidget {
  const LoadingSearchView(
    this._searchComponent, {
    super.key,
  });
  final SearchComponent _searchComponent;

  @override
  State<LoadingSearchView> createState() => _LoadingSearchViewState();
}

class _LoadingSearchViewState extends State<LoadingSearchView> {
  @override
  void initState() {
    super.initState();
    Future(() async {
      await Future.delayed(
        const Duration(seconds: 3),
      );
      widget._searchComponent.emitSucessState();
    });
  }

  @override
  Widget build(BuildContext context) {
    return const LinearProgressIndicator();
  }
}
