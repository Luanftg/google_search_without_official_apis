part of presentation;

class SearchView extends StatefulWidget {
  const SearchView(this._searchBloc, {super.key});
  final SearchComponent _searchBloc;

  @override
  State<SearchView> createState() => _SearchViewState();
}

final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
final TextEditingController searchController = TextEditingController();
final ValueNotifier<bool> _isFormValid = ValueNotifier<bool>(false);

class _SearchViewState extends State<SearchView> {
  @override
  void initState() {
    super.initState();
    _isFormValid.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        onChanged: () {
          _isFormValid.value = _formKey.currentState?.validate() ?? false;
        },
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const SizedBox(height: 16),
            const Text(
                'Você pode digitar um texto para uma pesquisa por links na página do Google',
                textAlign: TextAlign.center),
            const SizedBox(height: 32),
            TextFormField(
              controller: searchController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                constraints:
                    BoxConstraints(maxWidth: context.screenWidth * 0.6),
                label: const Text('Digite a sua Pesquisa'),
              ),
              validator: (value) {
                if (value == null || value.isEmpty || value.length < 2) {
                  return 'Deve conter ao menos 2 caracteres';
                }
                return null;
              },
            ),
            const SizedBox(height: 32),
            Container(
              constraints: BoxConstraints(maxWidth: context.screenWidth * 0.6),
              width: context.screenWidth * 0.5,
              child: ValueListenableBuilder(
                valueListenable: _isFormValid,
                builder: (context, value, child) {
                  if (value) {
                    return SizedBox(
                      width: context.screenWidth * 0.4,
                      child: ElevatedButton(
                        onPressed: () async {
                          await widget._searchBloc
                              .search(searchController.text);
                          searchController.clear();
                        },
                        child: const Text('Pesquisar'),
                      ),
                    );
                  }
                  return const SizedBox.shrink();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
