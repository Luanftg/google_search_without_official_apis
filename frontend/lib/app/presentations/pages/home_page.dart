part of presentation;

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

late SearchComponent _searchComponent;
final DependencyInjector di = DependencyInjector();

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    WidgetsFlutterBinding.ensureInitialized();
    _searchComponent = di.get<SearchComponent>();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      desktop: Scaffold(
        appBar: AppBar(
          backgroundColor: context.colors.primary.withOpacity(0.6),
          title: Text(widget.title),
          centerTitle: true,
        ),
        body: Center(
          child: SizedBox(
            width: context.screenWidth * 0.6,
            child: StreamBuilder(
              initialData: _searchComponent.initialData,
              stream: _searchComponent.state,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  switch (snapshot.data) {
                    case InitialSearchState():
                      return SearchView(_searchComponent);
                    case SucessSearchState():
                      final list = (snapshot.data as SucessSearchState).list;
                      return SucessSearchView(
                        list,
                      );
                    case LoadingSearchState():
                      return LoadingSearchView(_searchComponent);
                  }
                }
                return const SizedBox.shrink();
              },
            ),
          ),
        ),
      ),
      mobile: Scaffold(
        appBar: AppBar(
          backgroundColor: context.colors.primary.withOpacity(0.6),
          title: Text(widget.title),
          centerTitle: true,
        ),
        body: StreamBuilder(
          initialData: _searchComponent.initialData,
          stream: _searchComponent.state,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data) {
                case InitialSearchState():
                  return SearchView(_searchComponent);
                case SucessSearchState():
                  final list = (snapshot.data as SucessSearchState).list;
                  return SucessSearchView(
                    list,
                  );
                case LoadingSearchState():
                  return LoadingSearchView(_searchComponent);
              }
            }
            return const SizedBox.shrink();
          },
        ),
      ),
    );
  }
}
