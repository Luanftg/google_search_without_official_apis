part of presentation;

class ErrorText extends StatelessWidget {
  const ErrorText({super.key, required this.query});
  final String query;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Text.rich(
        TextSpan(
          text: 'Erro ao realizar a busca para:',
          children: [
            TextSpan(text: query, style: context.textStyles.errorText)
          ],
        ),
      ),
    );
  }
}
