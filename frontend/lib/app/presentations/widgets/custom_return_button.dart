part of presentation;

class CustomReturnButton extends StatelessWidget {
  const CustomReturnButton({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: context.screenWidth * 0.4,
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: ElevatedButton.icon(
          onPressed: _searchComponent.emitInitialState,
          icon: const Icon(Icons.arrow_back_ios_new_outlined),
          label: const Text('Nova Pesquisa'),
        ),
      ),
    );
  }
}
