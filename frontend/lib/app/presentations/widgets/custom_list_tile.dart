part of presentation;

class CustomListTile extends StatelessWidget {
  const CustomListTile({super.key, required this.title, required this.url});
  final String title;
  final String url;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      titleAlignment: ListTileTitleAlignment.center,
      title: Text(title, textAlign: TextAlign.center),
      subtitle: TextButton.icon(
        icon: const Icon(
          Icons.link,
          size: 15,
        ),
        onPressed: () async {
          await launchUrl(
            webOnlyWindowName: '_blank',
            Uri.parse(
              url,
            ),
          );
        },
        label: Text(
          maxLines: 2,
          url,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
