part of domain;

abstract class GetSearchByQueryUsecase {
  Future<List<SearchEntity>> call(String query);
}
