part of domain;

class GetSearchByQueryUsecaseImp implements GetSearchByQueryUsecase {
  final GetSearchByQueryRepository _getSearchByQueryRepository;

  GetSearchByQueryUsecaseImp(this._getSearchByQueryRepository);

  @override
  Future<List<SearchEntity>> call(String query) async {
    return await _getSearchByQueryRepository(query);
  }
}
