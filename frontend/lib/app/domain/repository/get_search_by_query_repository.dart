part of domain;

abstract class GetSearchByQueryRepository {
  Future<List<SearchEntity>> call(String query);
}
