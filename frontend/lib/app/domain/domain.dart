library domain;

part 'entities/search_entity.dart';
part 'usecase/get_search_by_query_usecase.dart';
part 'usecase/get_search_by_query_usecase_imp.dart';
part 'repository/get_search_by_query_repository.dart';
