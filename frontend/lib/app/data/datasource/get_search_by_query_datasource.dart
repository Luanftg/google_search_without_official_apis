part of data;

abstract class GetSearchByQueryDatasource {
  Future<List<SearchEntity>> call(String query);
}
