part of data;

class GetSearchByQueryDatasourceImp implements GetSearchByQueryDatasource {
  final HttpService _httpService;

  GetSearchByQueryDatasourceImp(this._httpService);

  @override
  Future<List<SearchEntity>> call(String query) async {
    final url = API.requestQuery(query);
    final response = await _httpService.get(url);
    List<SearchEntity> listOfSearchEntity = [];
    if (response.statusCode == 200) {
      final listOfMap = (response.data as List).map<Map>((e) => (e)).toList();
      listOfSearchEntity = SearchOutputDTO.toCollection(listOfMap);
    }
    return listOfSearchEntity;
  }
}
