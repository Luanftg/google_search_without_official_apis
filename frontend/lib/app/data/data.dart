library data;

import 'package:frontend/app/core/core.dart';

import '../core/api.dart';
import '../domain/domain.dart';

part 'dto/search_output_dto.dart';
part 'datasource/get_search_by_query_datasource.dart';
part 'datasource/get_search_by_query_datasource_imp.dart';
part 'repository/get_search_by_query_repository_imp.dart';
