part of data;

class GetSearchByQueryRepositoryImp implements GetSearchByQueryRepository {
  final GetSearchByQueryDatasource _getSearchByQueryDatasource;

  GetSearchByQueryRepositoryImp(this._getSearchByQueryDatasource);
  @override
  Future<List<SearchEntity>> call(String query) async {
    return await _getSearchByQueryDatasource(query);
  }
}
