part of data;

class SearchOutputDTO {
  final String title;
  final String url;

  SearchOutputDTO({required this.title, required this.url});

  static SearchEntity fromMap(Map map) {
    return SearchEntity(title: map['title'], url: map['url']);
  }

  static List<SearchEntity> toCollection(List<Map> list) {
    return list.map(SearchOutputDTO.fromMap).toList();
  }
}
