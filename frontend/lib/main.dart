import 'package:flutter/material.dart';
import 'package:frontend/app/core/core.dart';

import 'my_app.dart';

void main() async {
  await Injects.initialize();
  runApp(const MyApp());
}
