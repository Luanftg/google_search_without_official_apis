# :computer: Teste de Implementação - Processo Seletivo - ATAK Sistemas

## Artefatos desenvolvidos em Produção

- Os projetos podem ser acessados em produção:

- Link para acessar o projeto Web em produção
  - :star: [FrontEnd - Search App](http://search-front.luanftg.com.br/)
- Link para baixar o apk de instalação do projeto Android
  - :star: [APK - FrontEnd Search App - Mobile](https://drive.google.com/file/d/1049VgVIfYqnyV1o1Y-12hPf68GBkQYwI/view?usp=sharing)
- Url para acesso a API em produção
  - [Backend - Search App](http://search-api.luanftg.com.br:8080/search/dart)
    - Acesso ao endpoint:
    - `http://search-api.luanftg.com.br:8080/search/<busca>`
    - **Substitua `<busca>` pelo texto de busca**

### Instrução para acesso Local

- Os projetos tambem podem ser executados localmente com os comandos:
  - Backend
    - `cd backend && dart run bin/serve.dart`
  - FrontEnd
    - `cd frontend && flutter run`
    - escolher entre os devices: fisicos, emulador ou navegador.

## Desafio

- *1. Desenvolver, em qualquer linguagem, uma API que faz uma pesquisa no google e devolve o resultado em json e/ou XML.*
  
  - [x] **Não utilizar as API do Google, como Search API.**

  - [x] **Deve se extrair do resultado do google:**
    - Titulo
    - Link

- *2. Um cliente para consumir a API em qualquer linguagem/Plataforma:*

  - [x] **Apenas um campo para entrada de dados do valor a ser pesquisado;**
  - [x] **Um botão para disparar a pesquisa;**
  - [x] **Um container para exibir o resultado.**
  - [x] **Não pode ser utilizado IFrames, ou similares.**
  - [x] O Titulo deve ser exibido, sem link.
  - [x] O Link logo abaixo do Titulo, clicavel e abrir em uma nova janela.
  - [x] Não deve ser exibido mais nada da página do google. Todas as outras informações, cabeçalhos, rodapés, etc devem ser descartados.
  - [x] Devolver um zip ou link do repositório para acesso ao fonte e com instruções para podermos rodar seu código.

```
Exemplo do resultado:

Maringá - Wikipedia
[https://en.wikipedia.org/wiki/Maringá]


MARINGA.COM - O portal da cidade de Maringá - Paraná
[www.maringa.com/]


Prefeitura do Município de Maringá
[www.maringa.pr.gov.br/]

```

### Resolução

- Decisão de desenvolver todo o projeto em MonoRepositório hospedado no [GitLab - google_search_without_official_apis](https://gitlab.com/Luanftg/google_search_without_official_apis.git)
- Desenvolvimento do projeto utilizando `dart` como linguagem de programação
- Utilização do [Framework Flutter](https://flutter.dev/) para desenvolvimento nas plataformas: web e android
- Escolha de Clean Arquitecture como base arquitetural para manter o desacoplamento e os princípios [SOLID](https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898)
- Desenvolvimento de testes no backend utilizando [flutter-test](https://docs.flutter.dev/testing) e [mocktail](https://pub.dev/packages/mocktail)
- Hospedagem da aplicação frontend e backend no [Google Cloud Platform](https://cloud.google.com/?hl=pt-br)
- Registro do domínio para aplicação frontend em [Registro -BR](https://registro.br/)
  
#### Implementações

- Backend
  - Utilização do pacote [Beautiful Soup Dart](https://pub.dev/packages/beautiful_soup_dart) para leitura dos dados na estutura DOM
  - Implementação de Middleware para respostas com MIME TYPE : application/json
  - Utilização de Docker para buildar a imagem do backend - a imagem é pública e pode ser acessado via [Docker Hub - luanftg/gcp-2-search-api](https://hub.docker.com/repository/docker/luanftg/gcp-2-search-api/general)
  - Criação de stages e jobs para automatização de CI e CD com [GitLab](https://gitlab.com/)

- FrontEnd
  - Utilização de packages hospedados em repositório git pessoal
    - [dependency- injector](https://github.com/Luanftg/dependency_injector)
    - [responsive layout](https://github.com/Luanftg/responsive_layout)
  - Abordagem de gerencia de estado utilizando paradigma reativo com elementos nativos do Framework como `Stream, Sink e StreamController`

  - Web
    - home
  ![Alt text](pics/front_01.png)
    - busca
  ![Alt text](pics/front_vbusca.jpg)
    - validação
  ![Alt text](pics/front_valida%C3%A7%C3%A3o.jpg)
    - loading
  ![Alt text](pics/front_loading.jpg)
    - resultado
  ![Alt text](pics/front_result.jpg)

  - Android

### :exclamation: Observações para Instalação do APK :exclamation:

*Por padrão e por segurança, o Google bloqueia a instalação de apps de fontes que não sejam confiáveis. Portanto, antes de instalar um APK, é necessário permitir que ele seja executado através de um navegador, como o Google Chrome, por exemplo.* [Canal Tech](https://canaltech.com.br/android/como-instalar-um-apk-no-android/)

#### Siga os passos abaixo para realizar a instalação via APK

- Passo 1: abra as “Configurações” do seu celular e acesse a aba “Apps e notificações”.
- Passo 2: em seguida, expanda a opção “Avançado”.
- Passo 3: toque em “Acesso especial a apps”.
- Passo 4: na aba seguinte, selecione “Instalar apps desconhecidos”.
- Passo 5: com a lista de apps aberta, localize o “Google Chrome” ou outro navegador de sua preferência e abra-o.
- Passo 6: habilite a opção “Permitir desta fonte” e agora você poderá instalar um APK baixado no Google Chrome.
- Passo 7: agora você poderá baixar um APK de algum site, como o *WeTransfer*.

:exclamation: Clique no link abaixo para realizar o download do APK :exclamation:

- [APK - FrontEnd Search App](https://drive.google.com/file/d/1049VgVIfYqnyV1o1Y-12hPf68GBkQYwI/view?usp=sharing)

### :computer: DESENVOLVEDOR

<table align="center">
  <tr>
    <td align="center">
      <a href="https://github.com/Luanftg">
        <img src="https://avatars.githubusercontent.com/u/51548623?v=4" width="100px;" alt="Foto do Luan Fonseca Torralbo Gimenez"/><br>
        <sub>
          <b>Luan F. T. Gimenez</b>
        </sub>
      </a>
    </td>
    </tr>
</table>

<hr>

<div align="center" flex="row">

<h3> Tecnologias Utilizadas

<hr>

<img src="https://camo.githubusercontent.com/114aa59f6bfe1ff7ef3444fbb224078eb6a32c43f0ed03a6c0c3e6df67e049ec/68747470733a2f2f7777772e766563746f726c6f676f2e7a6f6e652f6c6f676f732f666c7574746572696f2f666c7574746572696f2d69636f6e2e737667" alt="flutter" width="40" height="40" data-canonical-src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" style="max-width: 100%;">flutter
<img src="https://camo.githubusercontent.com/d54cb8a71c6e700018b4d1390e6178d544f5713b618cb11e3d9513640a82d0c9/68747470733a2f2f7777772e766563746f726c6f676f2e7a6f6e652f6c6f676f732f646172746c616e672f646172746c616e672d69636f6e2e737667" alt="dart" width="40" height="40" data-canonical-src="https://www.vectorlogo.zone/logos/dartlang/dartlang-icon.svg" style="max-width: 100%;">dart

</div align="center">
